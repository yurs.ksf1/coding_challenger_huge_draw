class Drawing(object):

    def __init__(self, writer):
        """
        Init Drawing instance
        """

        self.writer = writer
        self.size_pixels = (20, 4)
        self.drawing = [
            ['-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-'],
            ['|',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','|'],
            ['|',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','|'],
            ['|',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','|'],
            ['|',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','|'],
            ['-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-']
        ]

    def _print_writer(self):
        """
        print in file draw
        """

        for column in self.drawing:
            line_draw = ''.join(column)
            line_write = '{}{}'.format(line_draw, '\n')
            self.writer.write(line_write)

    def _create_canvas(self, width, height):
        """
        Create a new canvas
        :param width: Int width of canvas, Ie, 20
        :param height: Int height of canvas, Ie, 4
        """

        new_drawing = []
        margin_top = ['-'] * (width + 2)

        new_drawing.append(margin_top)
        for i in range(height):
            line_display_blank = [' '] * (width + 2)
            line_display_blank[0] = '|'
            line_display_blank[-1] = '|'
            new_drawing.append(line_display_blank)
        new_drawing.append(margin_top)

        self.drawing = new_drawing
        self.size_pixels = (width, height)

    def _create_line(self, x1, y1, x2, y2):
        """
        Create a new line in canvas
        :param x1: Int, Position x begin line, Ie, 1
        :param y1: Int, Position y begin line, Ie, 2
        :param x2: Int, Position x end line, Ie, 6
        :param y2: Int, Position y end line, Ie, 2
        """

        # TODO: We should consider make a validation for input and size pixels
        new_drawing = self.drawing
        if x1 == x2:
            delta_y = abs(y2 - y1)
            for i in range(delta_y):
                if y2 > y1:
                    new_drawing[y1 + i][x1] = 'x'
                else:
                    new_drawing[y2 + i][x1] = 'x'
        else:
            delta_x = abs(x2 - x1)
            for i in range(delta_x):
                if x2 > x1:
                    new_drawing[y1][x1 + i] = 'x'
                else:
                    new_drawing[y1][x2 + i] = 'x'

        new_drawing[y1][x1] = 'x'
        new_drawing[y2][x2] = 'x'

        self.drawing = new_drawing

    def _create_rectangle(self, x1, y1, x2, y2):
        """
        Create a new line in canvas
        :param x1: Int, Position x begin rectangle, Ie, 16
        :param y1: Int, Position y begin rectangle, Ie, 1
        :param x2: Int, Position x end rectangle, Ie, 20
        :param y2: Int, Position y end rectangle, Ie, 3
        """

        self._create_line(x1, y1, x1, y2)
        self._create_line(x2, y1, x2, y2)
        self._create_line(x1, y1, x2, y1)
        self._create_line(x1, y2, x2, y2)

    def _bucket_fill(self, x, y, colour):
        """
        Bucket fill canvas in specific position (x,y)
        :param x: Int, Position x, Ie, 10
        :param y: Int, Position y, Ie, 3
        :param colour: String, color to fill, Ie, 'o'
        """
        previous_color = self.drawing[y][x]
        self.recursive_fill(x, y, colour, previous_color)

    def recursive_fill(self, x, y, colour, previous_color):
        """
        Recursive fill, record the neighbors of a pixel
        :param x: Int, Position x, Ie, 10
        :param y: Int, Position y, Ie, 3
        :param colour: String, color to fill, Ie, 'o'
        :param previous_color: String, previous color, Ie, ' '
        :return: None when is fill
        """

        if self.drawing[y][x] != previous_color:
            return None

        self.drawing[y][x] = colour
        self.recursive_fill(x - 1, y - 1, colour, previous_color)
        self.recursive_fill(x - 1, y, colour, previous_color)
        self.recursive_fill(x - 1, y + 1, colour, previous_color)

        self.recursive_fill(x, y - 1, colour, previous_color)
        self.recursive_fill(x, y + 1, colour, previous_color)

        self.recursive_fill(x + 1, y - 1, colour, previous_color)
        self.recursive_fill(x + 1, y, colour, previous_color)
        self.recursive_fill(x + 1, y + 1, colour, previous_color)

    def _validate_params(self, instruction, x, y, x1=None, y1=None):
        """
        Add validation to params
        :param x: Int, Position x, Ie, 10
        :param y: Int, Position y, Ie, 3
        :return: Boll, True or False
        """

        valid = Drawing._validate_integer_positive(x) and Drawing._validate_integer_positive(y)

        if instruction == 'C':
            return valid

        valid = valid and self._validate_size(x, y)

        if x1 != None or y1 != None:
            valid = valid and self._validate_size(x1, y1)

        return valid

    @staticmethod
    def _validate_integer_positive(param):
        """
        validate number is positive integer
        :param param: int or String, to validate Ie, 1
        :return: Bool True or False
        """

        return type(param) == int and param > 0

    def _validate_size(self, x, y):
        """
        Validation size of a canvas
        :param x: Int, Position x, Ie, 10
        :param y: Int, Position y, Ie, 3
        :return: Boll, True or False
        """

        return self.size_pixels[0] >= x and self.size_pixels[1] >= y

    @staticmethod
    def _clean_data_line(line):
        """
        Clean data from line
        :param line: String, line who clean Ie, C 20 4\n
        :return: List: split an clean data
        """

        values = line.split(' ')
        for i in range(len(values)):
            if '\n' in values[i]:
                values[i] = values[i][:-1]
            if values[i].isdigit():
                values[i] = int(values[i])
        return values

    def processing_line(self, line):
        """
        Processing Line read
        :param line: String, line Ie. 'C 20 4'
        """

        data = Drawing._clean_data_line(line)
        is_valid = True
        instruction = data[0]
        if instruction == 'C':
            width = data[1]
            height = data[2]
            is_valid = self._validate_params(instruction, width, height)
            if is_valid:
                self._create_canvas(
                    width=width,
                    height=height
                )

        elif instruction == 'L':
            x1 = data[1]
            y1 = data[2]
            x2 = data[3]
            y2 = data[4]

            is_valid = self._validate_params(instruction, x1, y1, x2, y2)
            if is_valid:
                self._create_line(
                    x1=x1,
                    y1=y1,
                    x2=x2,
                    y2=y2
                )

        elif instruction == 'R':
            x1 = data[1]
            y1 = data[2]
            x2 = data[3]
            y2 = data[4]
            is_valid = self._validate_params(instruction, x1, y1, x2, y2)
            if is_valid:
                self._create_rectangle(
                    x1=x1,
                    y1=y1,
                    x2=x2,
                    y2=y2
                )

        elif instruction == 'B':
            x = data[1]
            y = data[2]
            is_valid = self._validate_params(instruction, x, y)
            if is_valid:
                self._bucket_fill(
                    x=x,
                    y=y,
                    colour=data[3]
                )

        if is_valid:
            self._print_writer()
        else:
            message = '{}, it is an invalid operation \n'.format(data)
            print(message)
            self.writer.write(message)


def main():
    with open('output.txt', 'w') as writer:
        drawing = Drawing(writer)
        with open('input.txt', 'r') as reader:
            line = reader.readline()
            while line != '':
                drawing.processing_line(line)
                line = reader.readline()


if __name__ == "__main__":
    main()
